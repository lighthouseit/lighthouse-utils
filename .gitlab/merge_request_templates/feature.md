## Visão Geral
### O que é a feature?
(Descreva a feature)

### Como foi implementada?
(Descreva como foi implementada a feature)

### Que áreas da aplicação ela impacta?
(Descreva as áreas impactas e se houver, algum código que teve que ser alterado)

## Testes
### O que é necessário para testar?
(Descreva os pré-requisitos para testar a feature)

### Quais os passos para reproduzir?
(Descreva os passos e itens para testar)

## Informações Adicionais
(Informações adicionais sobre a feature, caso hajam)