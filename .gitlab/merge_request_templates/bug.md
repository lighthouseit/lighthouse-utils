## Visão Geral
### O que é o bug?
(Descreva o bug)

### Qual é o resultado esperado?
(Descreva o resultado esperado após resolução do bug)

### O que causava o bug?
(Descreva o que estava causando o bug)

### Como foi resolvido?
(Descreva como foi resolvido o bug)

## Testes
### O que é necessário para testar?
(Descreva os pré-requisitos para testar a resolução do bug)

### Como Testar?
(Descreva os passos e itens para testar)

## Informações Adicionais
(Informações adicionais sobre o bug, como prints e etc, caso hajam)