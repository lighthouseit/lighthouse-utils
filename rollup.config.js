/* eslint-disable prefer-template */
const { uglify } = require('rollup-plugin-uglify');
const pkg = require('./package.json');

const banner = '//  Lighthouse Utils v'
  + pkg.version
  + '\n'
  + '//  (c) 2018-'
  + new Date().getFullYear()
  + ' Lighthouse Team\n'
  + '//  Lighthouse Utils may be freely distributed under the MIT license.\n';

const config = {
  input: 'src/index.js',
  output: {
    format: 'cjs',
    banner,
  },
  plugins: [],
};

if (process.env.NODE_ENV === 'production') {
  config.plugins.push(
    uglify({
      compress: {
        pure_getters: true,
        unsafe: true,
        unsafe_comps: true,
        warnings: false,
      },
    }),
  );
}

module.exports = config;
