const capitalize = require('../src/capitalize');

describe('capitalize', () => {
  const texts = [
    'LOREM IPSUM IS SIMPLY',
    'LOREM    IPSUM IS                SIMPLY',
    'LOREM    IPSUM IS 1234 5 // SIMPLY',
    'LOREM IPSUM IS 1234 5 // SIMPLY ',
    ' LOREM IPSUM IS 1234 5 // SIMPLY',
  ];

  it('returns a capitalized texts', () => {
    texts.forEach((v) => {
      const result = capitalize(v);
      expect(result).toBeDefined();
    });
  });
});
