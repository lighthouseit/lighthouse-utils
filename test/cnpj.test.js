const { validateCNPJ } = require('../src/cnpj');

describe('validateCNPJ', () => {
  const invalid = [
    '00000000000',
    '11111111111',
    '22222222222',
    '33333333333',
    '44444444444',
    '55555555555',
    '66666666666',
    '77777777777',
    '88888888888',
    '99999999999',
    '10203040506',
  ];

  const valid = [
    '56.333.543/0001-85',
    '03.043.173/0001-85',
    '72175221000101',
    '41.634.674/0001-25',
  ];

  it('returns false for invalid CNPJs', () => {
    invalid.forEach((v) => {
      const result = validateCNPJ(v);
      expect(result).toBeFalsy();
    });
  });

  it('returns false for valid CNPJs', () => {
    valid.forEach((v) => {
      const result = validateCNPJ(v);
      expect(result).toBeTruthy();
    });
  });
});
