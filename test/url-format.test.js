const urlFormat = require('../src/url-format');

describe('urlFormat', () => {
  it('returns a string with the formatted url', () => {
    const url = 'https://api.pubg.com/shards/steam/players';
    const params = {
      'filter[playerNames]': 'Donnes',
    };
    const result = urlFormat(url, params);

    expect(result).toEqual(
      'https://api.pubg.com/shards/steam/players?filter%5BplayerNames%5D=Donnes',
    );
  });
});
