const { validateCCNumber, validateCCExpiry, getCCNumberInfo } = require('../src/credit-card');

describe('validateCCNumber', () => {
  const invalid = ['5441078066958502', '4716275671108308', '342092651826059', '4576918280038922'];

  const valid = ['5519386243006166', '4485260587157906', '341701938616811', '5066995686399453'];

  it('returns false for invalid CC Numbers', () => {
    invalid.forEach((v) => {
      const result = validateCCNumber(v);
      expect(result).toBeFalsy();
    });
  });

  it('returns false for valid CC Numbers', () => {
    valid.forEach((v) => {
      const result = validateCCNumber(v);
      expect(result).toBeTruthy();
    });
  });
});

describe('validateCCExpiry', () => {
  const invalid = ['30/19', '22/22', '00/2020', '55/2019'];

  const valid = ['09/27', '11/20', '01/22', '03/22'];

  it('returns false for invalid CC Expiration date', () => {
    invalid.forEach((v) => {
      const result = validateCCExpiry(v);
      expect(result).toBeFalsy();
    });
  });

  it('returns false for valid CC Expiration date', () => {
    valid.forEach((v) => {
      const result = validateCCExpiry(v);
      expect(result).toBeTruthy();
    });
  });
});

describe('getCCNumberInfo', () => {
  const number = '5485775006283622';
  const payload = {
    card: {
      niceType: 'Mastercard',
      type: 'mastercard',
      patterns: [[51, 55], [2221, 2229], [223, 229], [23, 26], [270, 271], 2720],
      gaps: [4, 8, 12],
      lengths: [16],
      code: { name: 'CVC', size: 3 },
      matchStrength: 2,
    },
    isPotentiallyValid: false,
    isValid: false,
  };

  it('returns a object with info for a given credit card number', () => {
    const result = getCCNumberInfo(number);
    expect(result).toMatchObject(payload);
  });
});
