# Lighthouse Utils

![Lighthouse Utils](https://i.imgur.com/sJnEY2T.png)

A javascript library used by Lighthouse projects

## Installation

```bash
$ yarn add lighthouse-utils
// ou com npm
$ npm install lighthouse-utils
```

## Test

```bash
$ yarn test
$ npm run test
```

## Usage

```javascript
import {
  capitalize,
  validateCPF,
  maskCPF,
  unmaskCPF,
  validateCNPJ,
  maskCNPJ,
  unmaskCNPJ,
  validateCCExpiry,
  validateCCNumber,
  getCCNumberInfo,
  initials,
  urlFormat,
  maskPhone,
  unmaskPhone,
  validatePhone,
} from "@lighthouseapps/utils";

capitalize("LIGHTHOUSE - CRIAMOS APPS INSCRIVEIS"); // Lighthouse - Criamos Apps Incriveis
validateCPF("1111222233334444"); // Validate a CPF number and returns a boolean
maskCPF('28832326280'); // Returns formatted CPF string -> 288.323.262-80
unmaskCPF('288.323.262-80') // Removes CPF mask -> 28832326280
validateCNPJ("72175221000101"); // Validate a CNPJ number and returns a boolean
maskCNPJ('17702663000150'); // Returns formatted CNPJ string -> 17.702.663/0001-50
unmaskCNPJ('17.702.663/0001-50') // Removes CNPJ mask -> 17702663000150
validateCCNumber("5598928852411791"); // Validate credit card number and returns a boolean
validateCCExpiry("12/22"); // Validate credit card expiration date and returns a boolean
getCCNumberInfo("5598928852411791"); // Returns a object with informations about CC Number
initials('Lighthouse apps'); // Returns first letter of each word -> "LA"
maskPhone('11999898887'); // Returns brazzilian formatted phone string -> (11) 99989-8887
unmaskPhone('(11) 99989-8887'); // Removes phone mask -> 11999898887
validatePhone('(11) 99989-8887') // Validate a phone number and returns a boolean
```

## Brazzilian-utils shortcuts

For the sake of convenience, we expose the following [Brazzilian-utils](https://brazilian-utils.com.br/) methods:

- formatCPF
- isValidCPF
- isValidCNPJ
- formatCNPJ
- isValidPhone
- isValidEmail
- isValidBoleto
- formatBoleto
