const { pickBy, negate, isNil } = require('lodash');
const qs = require('qs');

/**
 * Format a URL params
 *
 * @param {*} url The url to be formatted
 * @param {*} params The params to be formatted
 */
const urlFormat = (url, params) => {
  return `${url}?${qs.stringify(pickBy(params, negate(isNil)))}`;
};

module.exports = urlFormat;
