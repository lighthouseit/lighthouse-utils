const { isEmpty } = require('lodash');

/**
 * Get an initial letter of the first and last words of a string
 *
 * @param {*} str
 * @return {string}
 */
function initials(str) {
  const arr = `${str}`.split(' ').filter(n => !isEmpty(n));
  return `${arr[0][0]}${arr[arr.length - 1][0]}`.toUpperCase();
}

module.exports = initials;
