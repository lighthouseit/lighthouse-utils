const { isValidBoleto, formatBoleto } = require('@brazilian-utils/brazilian-utils');

module.exports = {
  isValidBoleto,
  formatBoleto,
};
