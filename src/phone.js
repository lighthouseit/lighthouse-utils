const { isValidPhone } = require('@brazilian-utils/brazilian-utils');

const unmaskPhone = (number) => {
  return number.replace(/([()-])/gm, '');
};

const maskPhone = (phone) => {
  return unmaskPhone(phone).replace(/([0-9]{2})([0-9]{5})([0-9]{4})/gm, '($1) $2-$3');
};

function validatePhone(phone) {
  let phoneNumber = phone;
  if (!phone) return false;

  if (/\(|\)|-/gm.test(phone)) {
    phoneNumber = unmaskPhone(phone);
  }
  return /(\(?\d{2}\)?\s?)(9\d{4}-?\d{4})/gm.test(phoneNumber);
}

module.exports = {
  maskPhone, unmaskPhone, validatePhone, isValidPhone,
};
